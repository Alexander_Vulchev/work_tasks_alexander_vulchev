How to get started with the project:

* After cloning the repository or downloading the zip file(whichever you preffer) import the project as **Maven project** in ide of your choice.Quick maven update on the project will solve errors if there are any 
* Check the application properties and adjust the username and password for the database access - you need to have MySql database in order to start the program
* No need for database creation, because there is (spring.jpa.hibernate.ddl-auto = update) which will generate the database and table according to the mapping in the java code.
  - Queries for test data:

    INSERT INTO `accountsdb`.`account` (`id`, `birth_date`, `email_address`, `first_name`, `last_name`)
    VALUES (1, sysdate(), 'test@mail.bg', 'testFirst', 'testLast');
 
    INSERT INTO `accountsdb`.`account` (`id`, `birth_date`, `email_address`, `first_name`, `last_name`)
    VALUES (2, sysdate(), 'real@abv.bg', 'anotherOne', 'anotherOne');

* When the application is started the url for access is: http://localhost:8080/accounts
* From then on it's all about what you do!

P.S You can test the functionalities of the application via the Selenium tests or the Unit tests!

