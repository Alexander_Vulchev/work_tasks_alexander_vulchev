package tests.selenium;

import static org.junit.Assert.assertEquals;

import java.util.logging.Logger;

import org.apache.commons.lang.RandomStringUtils;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

/**
 * Test class consisting of tests covering all functionality on the accounts
 * page
 * 
 * @author Alexander
 *
 */
public class AllFunctionalityTest {

	private static Logger log = Logger.getLogger(AllFunctionalityTest.class.getName());
	static WebDriver driver;

	@BeforeClass
	public static void invokeBrowser() {
		// Change the Chrome driver path if you are on a different device
		System.setProperty("webdriver.chrome.driver", "D:\\Hack\\75-chrome-driver\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().deleteAllCookies();
		driver.manage().window().maximize();
		driver.get("localhost:8080/accounts");
		log.info("Browser has been started");
	}

	@Test
	public void testTitle() throws InterruptedException {
		String actualTitle = driver.getTitle();
		String expectedTitle = "Accounts Management";
		Thread.sleep(2000);
		assertEquals(actualTitle, expectedTitle);
		log.info("<<<<< Title test SUCCESS >>>>>");
	}

	@Test
	public void testCreateAccount() throws InterruptedException {
		String firstName = createAccount();
		WebElement viewAccountsButton = driver.findElement(By.xpath("//*[@id=\"viewAccounts\"]"));
		viewAccountsButton.click();
		WebElement searchInputField = driver
				.findElement(By.xpath("//*[@id=\"datatable_col_reorder_filter\"]/label/input"));
		searchInputField.sendKeys(firstName);
		Thread.sleep(2000);
		log.info("<<<<< testCreateAccount SUCCESS >>>>>");
	}

	@Test
	public void testDeleteAccount() throws InterruptedException {
		String firstName = createAccount();
		WebElement viewAccountsButton = driver.findElement(By.xpath("//*[@id=\"viewAccounts\"]"));
		viewAccountsButton.click();
		WebElement searchInputField = driver
				.findElement(By.xpath("//*[@id=\"datatable_col_reorder_filter\"]/label/input"));
		searchInputField.sendKeys(firstName);
		Thread.sleep(2000);
		WebElement deleteAccountButton = driver.findElement(By.xpath("//*[@id=\"tableDeleteButton\"]"));
		deleteAccountButton.click();
		Thread.sleep(1000);
		log.info("<<<<< testDeleteAccount SUCCESS >>>>>");
	}

	@Test
	public void testUpdateAccount() throws InterruptedException {
		String firstName = createAccount();
		WebElement viewAccountsButton = driver.findElement(By.xpath("//*[@id=\"viewAccounts\"]"));
		viewAccountsButton.click();
		WebElement searchInputField = driver
				.findElement(By.xpath("//*[@id=\"datatable_col_reorder_filter\"]/label/input"));
		searchInputField.sendKeys(firstName);
		Thread.sleep(2000);
		WebElement editAccountButton = driver.findElement(By.xpath("//*[@id=\"tableEditButton\"]"));
		editAccountButton.click();
		Thread.sleep(1000);
		WebElement firstNameInputField = driver.findElement(By.xpath("//*[@id=\"firstNameId\"]"));
		firstNameInputField.clear();
		firstNameInputField.sendKeys("EDITED");
		WebElement lastNameInputField = driver.findElement(By.xpath("//*[@id=\"lastNameId\"]"));
		lastNameInputField.clear();
		lastNameInputField.sendKeys("EDITED");
		WebElement emailInputField = driver.findElement(By.xpath("//*[@id=\"emailAddressId\"]"));
		emailInputField.clear();
		emailInputField.sendKeys("EDITED@mail.bg");
		WebElement saveAccountButton = driver
				.findElement(By.xpath("//*[@id=\"exampleModalCenter\"]/div/div/div[2]/div/form/div[5]/button[2]"));
		saveAccountButton.click();
		log.info("<<<<< testUpdateAccount SUCCESS >>>>>");
	}

	private String createAccount() throws InterruptedException {
		WebElement createAccountButton = driver.findElement(By.xpath("//*[@id=\"newAccountButtonId\"]"));
		createAccountButton.click();
		Thread.sleep(2000);
		WebElement firstNameInputField = driver.findElement(By.xpath("//*[@id=\"firstNameId\"]"));
		String firstName = RandomStringUtils.randomAlphabetic(6);
		firstNameInputField.sendKeys(firstName);
		WebElement lastNameInputField = driver.findElement(By.xpath("//*[@id=\"lastNameId\"]"));
		String lastName = RandomStringUtils.randomAlphabetic(6);
		lastNameInputField.sendKeys(lastName);
		WebElement emailInputField = driver.findElement(By.xpath("//*[@id=\"emailAddressId\"]"));
		emailInputField.sendKeys("selenium@mail.bg");
		WebElement birthDateInputField = driver.findElement(By.xpath("//*[@id=\"birthDateId\"]"));
		birthDateInputField.sendKeys("01/01/2019");
		WebElement saveAccountButton = driver
				.findElement(By.xpath("//*[@id=\"exampleModalCenter\"]/div/div/div[2]/div/form/div[5]/button[2]"));
		saveAccountButton.click();
		Thread.sleep(2000);
		return firstName;
	}

	@AfterClass
	public static void closeBrowser() {
		driver.quit();
		System.gc();
		log.info("Chrome browser has been closed");
	}

}
