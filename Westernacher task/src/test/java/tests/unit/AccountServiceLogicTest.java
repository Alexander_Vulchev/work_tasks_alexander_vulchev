package tests.unit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import user.management.dao.AccountDao;
import user.management.model.Account;
import user.management.model.beans.AccountEditBean;
import user.management.service.AccountServiceImpl;

@RunWith(MockitoJUnitRunner.Silent.class)
public class AccountServiceLogicTest {

	@Mock
	private AccountDao accountDao;

	@InjectMocks
	private AccountServiceImpl accountServiceImpl;

	Account account1 = new Account();
	Account account2 = new Account();

	@Before
	public void setUp() {
		account1.setId(1L);
		account1.setFirstName("test1");
		account1.setLastName("testLast1");
		account1.setBirthDate(new Date());
		account1.setEmailAddress("test1@mail.bg");

		account2.setId(2L);
		account2.setFirstName("test2");
		account2.setLastName("testLast2");
		account2.setBirthDate(new Date());
		account2.setEmailAddress("test2@mail.bg");
	}

	@Test
	public void testGetAllAccounts() {
		List<Account> accountList = new ArrayList<>();
		accountList.add(account1);
		accountList.add(account2);

		Mockito.when(accountDao.getAll()).thenReturn(accountList);
		List<AccountEditBean> beanList = accountServiceImpl.getAllAccounts();
		Mockito.verify(accountDao, Mockito.times(1)).getAll();
		Mockito.verifyNoMoreInteractions(accountDao);
		assertFalse(beanList.isEmpty());
	}

	@Test
	public void testGetAccountById() {

		Mockito.when(accountDao.get(Mockito.anyLong())).thenReturn(account1);
		AccountEditBean resultBean = accountServiceImpl.getAccountById(1L);
		Mockito.verify(accountDao, Mockito.times(1)).get(Mockito.anyLong());
		Mockito.verifyNoMoreInteractions(accountDao);
		assertNotNull(resultBean);
		assertEquals(resultBean.getFirstName(), account1.getFirstName());
	}
	
	@Test
	public void testCreateOrUpdateAccount() {
		accountServiceImpl.createOrUpdateAccount(account1);
		Mockito.verify(accountDao,Mockito.times(1)).saveOrUpdate(account1);
	}
	
	@Test
	public void testDeleteAccountById() {
		accountServiceImpl.deleteAccountById(account2.getId());;
		Mockito.verify(accountDao,Mockito.times(1)).delete(account2.getId());
	}

	@After
	public void cleanUp() throws Throwable {
		finalize();
	}
	
	@Override
	protected void finalize() throws Throwable {
		super.finalize();
	}

}
