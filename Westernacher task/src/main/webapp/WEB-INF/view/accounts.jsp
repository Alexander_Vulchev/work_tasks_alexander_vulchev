<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">

<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script type="text/javascript" charset="utf8"
	src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.js"></script>
<title>Accounts Management</title>
</head>
<body>
<br>
	<div class="container">
	<div class="row">
			<div class="col-sm-2"></div>
			<div class="col-s8">
				<div class="text-center">
					<h3>Welcome to the accounts page! Here you can view all the existing accounts,
					create new ones or if you wish update and delete them!</h3>
				</div>
			</div>
			<div class="col-sm-2"></div>
		</div>
		<br>
		<div class="row">
			<div class="col-sm"></div>
			<div class="col-sm">
				<div class="text-center">
					<button type="button" class="btn btn-success" data-toggle="modal"
						data-target="#exampleModalCenter" id="newAccountButtonId">New Account</button>
					<button type="button" class="btn btn-info" id="viewAccounts">View
						Accounts</button>
				</div>
			</div>
			<div class="col-sm"></div>
		</div>
		<br>
		<div class="row">
			<div class="col-sm"></div>
			<div class="col-sm">
				<div class="text-center">
					<table class="table" id="datatable_col_reorder">
						<thead class="thead-light">
							<tr>
								<th>Id</th>
								<th>FirstName</th>
								<th>LastName</th>
								<th>Email</th>
								<th>BirthDayDate</th>
								<th>Operation</th>
							</tr>
						</thead>
						<tbody>

						</tbody>
					</table>
				</div>
			</div>
			<div class="col-sm"></div>
		</div>
	</div>

	<div class="modal fade" id="exampleModalCenter" tabindex="-1"
		role="dialog" aria-labelledby="exampleModalCenterTitle"
		aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalCenterTitle">Create
						new account</h5>
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="container">
					<div class="row">
						<div class="col-sm-2"></div>
						<div class="col-sm-8">
							<h4>
								<u>All fields are mandatory</u>
							</h4>
						</div>
						<div class="col-sm-2"></div>
						</div>
						<form action="/create/account" method="POST">
							<input type="hidden" id="hiddenId" name="id">
							<div class="row">
								<div class="col-sm-2"></div>
								<div class="form-group col-sm-8">
									<label for="firstNameId">First name:</label> <input type="text"
										class="form-control" id="firstNameId" name="firstName"
										required>
								</div>
								<div class="col-sm-2"></div>
							</div>
							<div class="row">
								<div class="col-sm-2"></div>
								<div class="form-group col-sm-8">
									<label for="lastNameId">Last name:</label> <input type="text"
										class="form-control" id="lastNameId" name="lastName" required>
								</div>
								<div class="col-sm-2"></div>
							</div>
							<div class="row">
								<div class="col-sm-2"></div>
								<div class="form-group col-sm-8">
									<label for="emailAddressId">Email address:</label> <input
										type="email" class="form-control" id="emailAddressId"
										placeholder="example@gmail.com" name="emailAddress" required>
								</div>
								<div class="col-sm-2"></div>
							</div>
							<div class="row">
								<div class="col-sm-2"></div>
								<div class="form-group col-sm-8">
									<label for="birthDateId">Birth date:</label> <input type="date"
										class="form-control" id="birthDateId" name="birthDate"
										required>
								</div>
								<div class="col-sm-2"></div>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-secondary"
									data-dismiss="modal">Close</button>
								<button type="submit" class="btn btn-primary">Save</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>

		<script>
		$('#viewAccounts').click(function (e) {
			testAjax();
	    })
	    
				$('#datatable_col_reorder').on('click', '#tableEditButton', function() {
					var accountId = $(this).closest("tr").find('td:first').text();
					var row = $(this).closest("tr"); 
					$.ajax({
						  type: "GET",
						  url: "/account/get",
						  data: {
							accountId: accountId
						  },
						  cache: false,
						  success: function(data){
							  document.getElementById("hiddenId").value = data.id;
							  document.getElementById("firstNameId").value = data.firstName;
							  document.getElementById("lastNameId").value = data.lastName;
							  document.getElementById("emailAddressId").value = data.email;
							  document.getElementById("birthDateId").value = data.birthDate;
							  $('.modal-title').empty();
							  $('.modal-title').text('Update existing account');
							  $('#exampleModalCenter').modal('show');
						  }
						});
				});
		
		$('#datatable_col_reorder').on('click', '#tableDeleteButton', function() {
			var accountId = $(this).closest("tr").find('td:first').text();
			var row = $(this).closest("tr"); 
			$.ajax({
				  type: "DELETE",
				  url: "/account/delete?accountId=" + accountId,
				  cache: false,
				  success: function(data){
					  alert(JSON.stringify('Account deleted successfully'));
					  testAjax();
				  }
				});
		});
		
			$('#newAccountButtonId').click(function(e) {
				$('#firstNameId').val('');
				$('#lastNameId').val('');
				$('#emailAddressId').val('');
				$('#birthDateId').val('');
				$('.modal-title').empty();
				$('.modal-title').text('Create a new account');
			})
			
			function testAjax() {
				$.ajax({
					  type: "GET",
					  url: "/account/all",
					  data: "",
					  cache: false,
					  success: function(data){
		                    var table =$('#datatable_col_reorder').DataTable ( {
		                        data:data,
		                        columns: [
		                        	{"data": "id"},
		                            {"data": "firstName"},
		                            {"data": "lastName"},
		                            {"data": "email"}, 
		                            {"data": "birthDate"},
		                            {"defaultContent": "<button type='button' class='btn btn-primary btn-sm' id='tableEditButton'>Edit</button> <button type='button' class='btn btn-danger btn-sm' id='tableDeleteButton'>X</button>"}
		                        ],
		                       bDestroy: true
		                    });
					  }
					});
			}
		</script>
</body>
</html>

