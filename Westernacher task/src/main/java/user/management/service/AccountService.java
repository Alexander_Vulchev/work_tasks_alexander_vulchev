package user.management.service;

import java.util.List;

import user.management.model.Account;
import user.management.model.beans.AccountEditBean;

public interface AccountService {

	/**
	 * Retrieves all accounts that are present in the database
	 * 
	 * @return List<AccountEditBean>
	 * @see Account
	 */
	List<AccountEditBean> getAllAccounts();

	/**
	 * Creates a new account entity if the enitity does not exists or updates one if
	 * it already exists and saves it in the database
	 * 
	 * @param account of type Account
	 */
	void createOrUpdateAccount(Account account);

	/**
	 * Extracts from the database an account corresponding to the given id
	 * 
	 * @param accountId of type Long
	 * @return AccountEditBean
	 */
	AccountEditBean getAccountById(Long accountId);

	/**
	 * Deletes account from the database with the id given
	 * 
	 * @param accountId of type Long
	 */
	void deleteAccountById(Long accountId);

}
