package user.management.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import user.management.dao.AccountDao;
import user.management.model.Account;
import user.management.model.beans.AccountEditBean;

@Service
public class AccountServiceImpl implements AccountService {

	@Autowired
	private AccountDao accountDao;

	@Override
	public List<AccountEditBean> getAllAccounts() {
		List<Account> accountList = accountDao.getAll();
		List<AccountEditBean> beanList = new ArrayList<>();
		for (Account account : accountList) {
			AccountEditBean bean = populateAccountBean(account);
			beanList.add(bean);
		}
		return beanList;
	}

	@Override
	public void createOrUpdateAccount(Account account) {
		accountDao.saveOrUpdate(account);
	}

	@Override
	public AccountEditBean getAccountById(Long accountId) {
		Account acc = accountDao.get(accountId);
		AccountEditBean resultBean = populateAccountBean(acc);
		return resultBean;
	}

	private AccountEditBean populateAccountBean(Account acc) {
		AccountEditBean resultBean = new AccountEditBean();
		resultBean.setId(acc.getId());
		resultBean.setFirstName(acc.getFirstName());
		resultBean.setLastName(acc.getLastName());
		resultBean.setEmail(acc.getEmailAddress());
		resultBean.setBirthDate(new SimpleDateFormat("yyyy-MM-dd").format(acc.getBirthDate()));
		return resultBean;
	}

	@Override
	public void deleteAccountById(Long accountId) {
		accountDao.delete(accountId);
	}

}
