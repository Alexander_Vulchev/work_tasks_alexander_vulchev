package user.management.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import user.management.model.Account;
import user.management.model.beans.AccountEditBean;
import user.management.service.AccountService;

@RestController
@RequestMapping("/account")
public class AccountsApi {

	@Autowired
	private AccountService accountService;

	@GetMapping("/all")
	public ResponseEntity<List<AccountEditBean>> getAccounts() {
		return ResponseEntity.ok(accountService.getAllAccounts());
	}

	@GetMapping("/get")
	public ResponseEntity<AccountEditBean> getAccountById(Long accountId) {
		return ResponseEntity.ok(accountService.getAccountById(accountId));
	}

	@DeleteMapping("/delete")
	public ResponseEntity<List<Account>> deleteAccount(@RequestParam(name = "accountId") Long accountId) {
		accountService.deleteAccountById(accountId);
		return ResponseEntity.ok().build();
	}

}
