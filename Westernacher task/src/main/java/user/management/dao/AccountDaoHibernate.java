package user.management.dao;

import org.springframework.stereotype.Repository;

import user.management.model.Account;

@Repository
public class AccountDaoHibernate extends GenericDaoImpl<Account> implements AccountDao {

}
