package user.management.dao;

import java.io.Serializable;
import java.util.List;

public interface GenericDao<T> {

	/**
	 * Saves the given object to the database
	 * 
	 * @param o
	 * @return
	 */
	Serializable save(final T o);

	/**
	 * Deletes an object from the database based on provided id
	 * 
	 * @param id
	 */
	void delete(final Long id);

	/**
	 * Extracts object from the database with the given id
	 * 
	 * @param id
	 * @return
	 */
	T get(final Long id);

	/**
	 * Updates object from the database with the given id
	 * 
	 * @param o
	 */
	void saveOrUpdate(final T o);

	/**
	 * Returns a list of all the objects from the database
	 * 
	 * @return
	 */
	List<T> getAll();

}
