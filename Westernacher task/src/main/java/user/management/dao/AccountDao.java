package user.management.dao;

import user.management.model.Account;

public interface AccountDao extends GenericDao<Account> {

}
