package user.management.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import user.management.model.Account;
import user.management.service.AccountService;

@Controller
public class AccountsController {

	@Autowired
	private AccountService accountService;

	@GetMapping("/accounts")
	public String toAccountsPage() {
		return "accounts";
	}

	@PostMapping("/create/account")
	public String createAccount(Account account) {
		accountService.createOrUpdateAccount(account);
		return "redirect:/accounts";
	}

}
